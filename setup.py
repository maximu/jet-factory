#!/usr/bin/env python
from setuptools import setup

setup(name='JetFactory',
      version='1.0',
      description='',
      author='Azkali Manad',
      author_email='a.ffcc7@gmail.com',
      url='https://gitlab.azka.li/l4t-community/gnu-linux/jet-factory.git',
      install_requires=[
        "patool",
        "clint",
        "sh",
        "requests",
        "PyInquirer",
        "envbash",
        "rich",
        "pyyaml"
      ]
)
