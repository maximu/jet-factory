import abc
from jetfactory.jetfactory import LinuxFactory
from sh import debootstrap
from os import environ, stat, remove
from os.path import exists
from urllib.request import urlopen, Request
import sys
import glob

class Rootfs(LinuxFactory):
    ''' 
        Description:
            Meta Rootfs class that any rootfs must inherit this class.

        Available variables:
            name: str           # Represent the rootfs name
            url: str            # URL pointing to either a rootfs archive or disk image
            MIRROR: str         # Used by debootstrap. Sets the right mirror to use (ubuntu, debian, other)
            MANAGER: str        # Package manager of the distribution
            INSTALL: str        # Package manager install command
            PRE: Iterable[str]  # List of commands to run during preConfigs stage
            POST: Iterable[str] # List of commands to run during preConfigs stage
            PACKAGES: str       # Package list separeted by spaces
            OPTIONS: str        # Additional options passed to package manager
    '''
    __metaclass__ = abc.ABCMeta
    OPTIONS = ""

    def prepare(self):
        self.extractRootfsFromURL()
        self.mkChroot()

    def downloadCheckUrl(self, url):
        path = self.dl_dir + "/" + url.split("/")[-1]

        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9,fr;q=0.8",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
        }

        res = urlopen(Request(url, headers=headers))
        urlContentSize = int(res.info()["Content-length"])
        sizeCheck = stat(path).st_size - int(res.info()["Content-length"]) if exists(path) else 0

        if sizeCheck != 0:
            self.logger.info("Removing corrupted previously downloaded file")
            remove(path)

        if not exists(path):
            self.download([url], self.dl_dir)

    def extractRootfsFromURL(self):
        if hasattr(self, "url"):
            self.path = self.dl_dir + "/" + self.url.split("/")[-1]
            self.downloadCheckUrl(self.url)
            self.extract_rootfs(self.path)

    def mkChroot(self):
        if hasattr(self, "path"):
            self.createRootfs()
        else:
            self.debootstrap()
        self.build()

    def update(self):
        '''
            Description: Run update command in rootfs.
            Required class variables: MANAGER, UPDATE
        '''
        self.run(self.MANAGER + " " + self.UPDATE + " " + self.OPTIONS)

    def upgrade(self):
        '''
            Description: Run upgrade command in rootfs.
            Required class variables: MANAGER, UPGRADE
        '''
        self.run(self.MANAGER + " " + self.UPGRADE + " " + self.OPTIONS)

    def configs(self, cmds):
        '''
            Description: Run arbitrary command.
        '''
        for cmd in cmds:
            self.run(cmd)

    def preConfigs(self):
        '''
            Description: Run arbitrary command after package installation.
            Required class variable: POST
        '''
        if hasattr(self, "PRE"):
            self.configs(self.PRE)

    def postConfigs(self):
        '''
            Description: Run arbitrary command before package installation.
            Required class variable: POST
        '''
        if hasattr(self, "POST"):
            self.configs(self.POST)

    @abc.abstractmethod
    def addRepo(self):
        '''
            Description:
                User defined abstract method.
                Add a repository to the rootfs
        '''
        pass

    @abc.abstractmethod
    def install(self, component):
        '''
            Description: Installs packages to the rootfs.
            Required class variables: MANAGER, INSTALL, PACKAGES
        '''
        self.run(self.MANAGER + " " + self.INSTALL + " " + self.OPTIONS + " " + component)

    @abc.abstractmethod
    def installPackages(self):
        '''
            Description: Installs packages to the rootfs.
            Required class variables: MANAGER, INSTALL, PACKAGES
        '''
        self.install(self.PACKAGES)

    def createRootfs(self):
        '''
            Description: Default 
        '''
        with self.chroot():
            self.preConfigs()
            self.addRepo()
            self.update()
            self.upgrade()
            self.update()
            self.installPackages()
            self.postConfigs()

    def debootstrap(self):
        '''
            Description: Debootstrap command.
            Required class variables: PACKAGES, MIRROR
        '''
        self.PACKAGES = self.PACKAGES.replace(" ", ",")
        logpath = glob.glob('/tmp/*-chroot/debootstrap/debootstrap.log')
        try:
            debootstrap(
                "--resolve-deps",
                "--include=" + self.PACKAGES,
                "--arch=arm64",
                "--components=" + self.COMPONENTS,
                self.name,
                self.chroot_dir,
                self.MIRROR,
                _log_msg=self.sh_log,
                _out=sys.stdout
            )
        except:
            for fp in logpath:
                with open(fp, 'r') as log:
                    print(log.read())

            with self.chroot():
                if environ.get("JETFACTORY_INTERACTIVE") is not None:
                    self.run("/bin/bash")

            exit()

        self.createRootfs()

class Fedora(Rootfs):
    ''' Fedora Rootfs base class '''
    name = "fedora"
    rootfs = "fedora"
    url = "https://download.fedoraproject.org/pub/fedora/linux/releases/39/Workstation/aarch64/images/Fedora-Workstation-39-1.5.aarch64.raw.xz"
    MANAGER = "dnf"
    OPTIONS = "-y --refresh"
    INSTALL = "install --allowerasing"
    UPGRADE = "upgrade"

    def swap(self, src, dst):
        self.run(self.MANAGER + " swap " + self.OPTIONS + " " + src + " " + dst)

    def distro_sync(self):
        self.run(self.MANAGER + " distro-sync " + self.OPTIONS)

    def remove(self, packages):
        self.run(self.MANAGER + " remove " + self.OPTIONS + " " + packages)

    def update(self):
        self.upgrade()

    def upgrade(self):
        super().upgrade()

    def install(self, component):
        super().install(component)

    def installPackages(self):
        super().installPackages()

    def installRpmFusion(self):
        self.install("https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm")
        self.install("https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm")

    def addRepo(self, url):
        self.run(self.MANAGER + " config-manager addrepo " + self.OPTIONS + " --from-repofile=" + url)

    def cleanup(self):
        self.run(self.MANAGER + " clean all " + self.OPTIONS)
        self.run("history -c")

class Debian(Rootfs):
    ''' Debian Rootfs base class '''
    name = "bookworm"
    rootfs = "debian"
    PRE = [
        "ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime",
	    "TZ=America/New_York ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone"
    ]
    MIRROR = "http://deb.debian.org/debian/"
    MANAGER = "DEBIAN_FRONTEND=noninteractive apt -y"
    INSTALL = "install"
    UPDATE = "update"
    UPGRADE = "upgrade"
    COMPONENTS = "main,contrib,non-free"
    PACKAGES = "task-gnome-desktop wget curl"
    cache = "/var/cache/apt/archives"

    def remove(self, packages):
        self.run(self.MANAGER + " remove " + packages)

    def update(self):
        super().update()

    def upgrade(self):
        super().upgrade()

    def install(self, component):
        super().install(component)

    def installPackages(self):
        super().installPackages()

    def addRepo(self, url):
        self.run("add-apt-repository " + url)

class Ubuntu(Debian):
    ''' Ubuntu Rootfs base class '''
    name = "ubuntu"
    rootfs = "ubuntu"
    MIRROR = "http://ports.ubuntu.com/ubuntu-ports"
    COMPONENTS = "main,restricted,universe,multiverse"

class Focal(Ubuntu):
    ''' Ubuntu Focal Rootfs '''
    name = "focal"
    rootfs = "focal"

class Arch(Rootfs):
    ''' Arch Rootfs default class '''
    name = "arch"
    rootfs = "arch"
    url = "http://os.archlinuxarm.org/os/ArchLinuxARM-aarch64-latest.tar.gz"
    PRE = [
        "pacman-key --init",
        "pacman-key --populate archlinuxarm",
        "sed -i 's/CheckSpace*/# CheckSpace/g' /etc/pacman.conf"
    ]
    MANAGER = "pacman --noconfirm"
    INSTALL = "-Syy"
    UPDATE = "-Syyu"
    cache = "/var/cache/pacman/pkg"
