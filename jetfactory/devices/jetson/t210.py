import os
from jetfactory.jetfactory import LinuxFactory
from jetfactory.rootfs import Fedora
from shutil import rmtree, copyfile
from sh import du

class T210(LinuxFactory):
    ''' T210 device base class '''
    device = "nano"
    BSP = "https://developer.nvidia.com/downloads/embedded/l4t/r32_release_v7.4/t210/jetson-210_linux_r32.7.4_aarch64.tbz2"
    DEVICE_NAME = "jetson-nano"
    REVISION = "300"
    PARTITION = "mmcblk0p1"

    def prepare(self):
        LinuxFactory.prepare(self)
        rmtree(self.dl_dir + "/Linux_for_Tegra/", ignore_errors=True)
        self.downloadCheckUrl(self.BSP)
        bspPath = self.dl_dir + "/" + self.BSP.split("/")[-1]
        self.extract(bspPath, self.dl_dir)

    def makeImage(self, disk_name):
        size = 256 + int(du(
            "-s",
            "-BM",
            self.chroot_dir,
            _log_msg=self.sh_log)
            .split("M")[0])
        self.patchBSP()
        os.chdir(self.dl_dir + "/Linux_for_Tegra/tools/")
        self.run("sudo ./jetson-disk-image-creator.sh -o {} -b {} -r {} -s {}M"
            .format(disk_name, self.DEVICE_NAME , self.REVISION, size))

    def patchBSP(self):
        flash = open(self.dl_dir + "/Linux_for_Tegra/bootloader/tegraflash_internal.py", "r")
        flash_w = open(self.dl_dir + "/Linux_for_Tegra/bootloader/tegraflash_internal.py", "w")
        flash_w.write(flash.read().replace("getiterator", "iter"))
        flash.close()
        flash_w.close()

    # TODO: Prevent kernel/modules being copied
    def build(self):
        disk_name = self.build_dir + "/" + self.name + "-" + self.DEVICE_NAME + "-" + self.REVISION + ".img"
        self.logger.info("Creating disk image: {} for device: {} revision {}"
                         .format(disk_name, self.DEVICE_NAME, self.REVISION))

        os.makedirs(self.dl_dir + "/Linux_for_Tegra/rootfs/boot/extlinux/", exist_ok=True)
        copyfile(self.dl_dir + "/Linux_for_Tegra/bootloader/extlinux.conf", self.dl_dir + "/Linux_for_Tegra/rootfs/boot/extlinux/extlinux.conf")
        self.makeImage(disk_name)

class FedoraT210(T210, Fedora):
    ''' T210 Fedora Rootfs override '''
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_39/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_39/home:Azkali:Tegra.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Apps/Fedora_39/home:Azkali:Apps.repo",
        "https://nvidia.github.io/libnvidia-container/stable/rpm/nvidia-container-toolkit.repo"
    ]
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*,x264-libs-*git*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel' | xargs rpm -e --noscripts --nodeps",
        "usermod -aG video,audio,input gdm"
    ]
    PACKAGES = "switch-meta-nano gnome-shell-extension-appindicator gnome-extensions-app gnome-shell-extension-dash-to-dock"

    def prepare(self):
        T210.prepare(self)
        self.chroot_dir = self.dl_dir + "/Linux_for_Tegra/rootfs/"
        Fedora.prepare(self)

    def extractRootfsFromURL(self):
        if hasattr(self, "url"):
            self.path = self.dl_dir + "/" + self.url.split("/")[-1]
            self.downloadCheckUrl(self.url)
            self.extract_rootfs(self.path, self.chroot_dir)

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.update()
            self.remove("firefox totem gnome-weather gnome-maps gnome-calendar gnome-tour cheese gnome-shell-extension-background-logo edk2* uboot-images-armv8 podman libpinyin kernel-devel libreoffice-core httpd")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.installPackages()
            self.swap("nvidia-l4t-bsp", "nvidia-l4t-bsp-7.4")
            self.run("systemctl enable initial-setup")
            self.cleanup()

class FedorakdeT210(FedoraT210):
    ''' Fedora T210 KDE spin '''
    rootfs = "kde"
    url = "https://download.fedoraproject.org/pub/fedora-secondary/releases/39/Spins/aarch64/images/Fedora-KDE-39-1.5.aarch64.raw.xz"
    PACKAGES = "switch-meta-nano openbox nvidia-container-toolkit"
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*,x264-libs-*git*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^initial-setup' | xargs rpm -e --noscripts --nodeps",
        "usermod -aG video,audio,input sddm"
    ]

    def prepare(self):
        T210.prepare(self)
        self.chroot_dir = self.dl_dir + "/Linux_for_Tegra/rootfs/"
        Fedora.prepare(self)

    def extractRootfsFromURL(self):
        if hasattr(self, "url"):
            self.path = self.dl_dir + "/" + self.url.split("/")[-1]
            self.downloadCheckUrl(self.url)
            self.extract_rootfs(self.path, self.chroot_dir)

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.update()
            self.remove("firefox uboot-images-armv8 kernel-devel httpd imsettings akregator kamoso mediawriter elisa-player kmag kgpg qt5-qdbusviewer kcharselect kcolorchooser dragon kmines kmahjongg kpat kruler kmousetool kmouth kolourpaint konversation krdc kfind kaddressbook kmail kontact korganizer ktnef krfb kf5-akonadi-*")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.installPackages()
            self.run("systemctl enable initial-setup l4t-firstboot")
            self.cleanup()
