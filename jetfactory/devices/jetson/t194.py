import os
from jetfactory.jetfactory import LinuxFactory
from jetfactory.rootfs import Fedora
from shutil import rmtree

class T194(LinuxFactory):
    ''' T194 device base class '''
    device = "xavier"
    BSP = "https://developer.download.nvidia.com/embedded/L4T/r35_Release_v2.1/release/Jetson_Linux_R35.2.1_aarch64.tbz2"
    DEVICE_NAME = "jetson-xavier-nx-devkit"
    PARTITION = "mmcblk0p1"

    def prepare(self):
        LinuxFactory.prepare(self)
        rmtree(self.dl_dir + "/Linux_for_Tegra/", ignore_errors=True)
        self.downloadCheckUrl(self.BSP)
        bspPath = self.dl_dir + "/" + self.BSP.split("/")[-1]
        self.extract(bspPath, self.dl_dir)

    def makeImage(self):
        self.run("./tools/jetson-disk-image-creator.sh -o {} -b {}"
                .format(disk_name, self.DEVICE_NAME))
        os.rename("./" + disk_name, self.build_dir + disk_name)

    # TODO: Prevent kernel/modules being copied
    def build(self):
        disk_name = self.DEVICE_NAME + "-" + self.name + "-" + self.name + ".img"
        self.logger.info("Creating disk image: {} for device: {}"
                         .format(disk_name, self.DEVICE_NAME))

        try:
            os.chdir(self.dl_dir + "/Linux_for_Tegra/")
            os.makedirs("rootfs/boot/extlinux/", exist_ok=True)
            os.copy("bootloader/extlinux.conf", "rootfs/boot/extlinux/")
            self.makeImage()

        except:
            self.logger.info("Something went wrong during image creation \
                ./tools/jetson-disk-image-creator.sh -o {} -b {}"
                        .format(disk_name, self.DEVICE_NAME))

class FedoraT194(T194, Fedora):
    ''' T194 Fedora Rootfs override '''
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Xavier/Fedora_37/home:Azkali:Tegra:Xavier.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_37/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_37/home:Azkali:Tegra.repo"
    ]
    PRE = [
        "touch /.initial-setup",
        "echo 'exclude=kernel-core*,kernel-modules*,pipewire-pulseaudio*,x264-libs-*git*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^pipewire-pulseaudio' | xargs rpm -e --noscripts --nodeps"
    ]
    PACKAGES = "jetson-xavier-meta gnome-shell-extension-appindicator gnome-extensions-app gnome-shell-extension-dash-to-dock gnome-shell-extension-improved-osk"

    def prepare(self):
        T194.prepare(self)
        self.chroot_dir = self.dl_dir + "/Linux_for_Tegra/rootfs/"
        Fedora.prepare(self)

    def extractRootfsFromURL(self):
        if hasattr(self, "url"):
            self.path = self.dl_dir + "/" + self.url.split("/")[-1]
            self.downloadCheckUrl(self.url)
            self.extract_rootfs(self.path, self.chroot_dir)

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.remove("firefox totem gnome-weather gnome-maps gnome-calendar gnome-tour cheese gnome-shell-extension-background-logo")
            self.preConfigs()
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.swap("kernel*", "l4t-kernel*")
            self.installPackages()

class T194AGX(T194):
    device = "xavier-agx"
    DEVICE_NAME = "jetson-agx-xavier-devkit"

class FedoraT194AGX(FedoraT194):
    ''' T194 AGX Fedora Rootfs override '''
