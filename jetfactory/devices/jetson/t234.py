from jetfactory.devices.jetson.t194 import T194, FedoraT194

class T234(T194):
    ''' T234 device base class '''
    device = "orin-agx"
    DEVICE_NAME = "jetson-agx-orin-devkit"

class FedoraT234(FedoraT194):
    ''' T234 Fedora Rootfs override '''

    def makeImage(self):
        self.run("./tools/jetson-disk-image-creator.sh -o {} -b {} -d SD"
                .format(disk_name, self.DEVICE_NAME))
        os.rename("./" + disk_name, self.build_dir + disk_name)
