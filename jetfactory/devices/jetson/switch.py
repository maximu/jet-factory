import os
from jetfactory.jetfactory import LinuxFactory
from jetfactory.rootfs import Fedora, Arch, Focal, Debian
from patoolib import create_archive
from sh import split

class Switch(LinuxFactory):
    ''' Switch meta device '''
    device = "switch"

    def prepare(self):
        self.logger.info("Preparing switch directories")
        self.out = self.distro_dir + "/switchroot/" + self.name + "/"
        os.makedirs(self.out, exist_ok=True)
        os.makedirs(self.distro_dir + "/switchroot/install/", exist_ok=True)
        os.makedirs(self.distro_dir + "/bootloader/ini/", exist_ok=True)

    def splitImage(self):
        ''' Split disk image '''
        self.logger.info("Spliting {}".format(self.disk_name))
        split("-b4290772992",
              "--numeric-suffixes=0",
              self.disk_name,
              self.distro_dir + "/switchroot/install/l4t.",
              _log_msg=self.sh_log
        )
        os.remove(self.disk_name)

class FedoraSwitch(Switch, Fedora):
    ''' Switch Fedora Rootfs override '''
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_39/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_39/home:Azkali:Tegra.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Apps/Fedora_39/home:Azkali:Apps.repo",
        "https://nvidia.github.io/libnvidia-container/stable/rpm/nvidia-container-toolkit.repo"
    ]
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*,x264-libs-*git*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel' | xargs rpm -e --noscripts --nodeps",
        "usermod -aG video,audio,input gdm"
    ]
    PACKAGES = "switch-meta gnome-shell-extension-appindicator gnome-extensions-app gnome-shell-extension-dash-to-dock chromium-nvdec nvidia-l4t-bsp-7.4"

    def prepare(self):
        LinuxFactory.prepare(self)
        Switch.prepare(self)
        Fedora.prepare(self)

    def build(self):
        ''' Create a 7z archive containing a splitted image file to fit on switch's fat32 PARTITION '''
        LinuxFactory.build(self)
        self.splitImage()
        create_archive(self.zip_name, [self.distro_dir + "/switchroot/", self.distro_dir + "/bootloader/"])

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.update()
            self.remove("totem gnome-weather gnome-maps gnome-calendar gnome-tour cheese gnome-shell-extension-background-logo edk2* uboot-images-armv8 podman libpinyin kernel-devel libreoffice-core httpd")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.swap("kernel*", "switch-kernel*")
            self.installPackages()
            self.run("systemctl enable initial-setup l4t-firstboot")
            self.cleanup()

class FedoranouveauSwitch(FedoraSwitch):
    ''' Fedora Switch Nouveau kernel '''
    name = "fedoranouveau"
    rootfs = "nouveau"
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_39/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa_testing/Fedora_39/home:Azkali:Tegra:Icosa_testing.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_39/home:Azkali:Tegra.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Apps/Fedora_39/home:Azkali:Apps.repo"
    ]
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*,mesa*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^mesa' | xargs rpm -e --noscripts --nodeps"
    ]
    PACKAGES = "switch-mainline-meta gnome-shell-extension-appindicator gnome-extensions-app gnome-shell-extension-dash-to-dock gnome-shell-extension-cariboublocker nvidia-container-toolkit"

    def build(self):
        LinuxFactory.build(self)
        self.splitImage()
        create_archive(self.zip_name, [self.distro_dir + "/switchroot/", self.distro_dir + "/bootloader/"])

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.remove("firefox totem gnome-weather gnome-maps gnome-calendar gnome-tour cheese gnome-shell-extension-background-logo edk2* uboot-images-armv8 podman libpinyin kernel-devel libreoffice-core httpd")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.swap("kernel*", "switch-kernel*")
            self.installPackages()
            self.cleanup()
            if os.environ.get("JETFACTORY_DEBUG") is not None:
                self.run("systemctl disable initial-setup")
                self.run("adduser -s /bin/bash -m -G wheel,video,audio,input,kvm fedora")
                self.run("echo 'fedora:fedora' | chpasswd")

class FedorakdeSwitch(FedoraSwitch):
    ''' Fedora Switch KDE spin '''
    rootfs = "kde"
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_41/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_41/home:Azkali:Tegra.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Apps/Fedora_41/home:Azkali:Apps.repo",
        "https://nvidia.github.io/libnvidia-container/stable/rpm/nvidia-container-toolkit.repo"
    ]
    url = "https://download.fedoraproject.org/pub/fedora/linux/releases/41/Spins/aarch64/images/Fedora-KDE-41-1.4.aarch64.raw.xz"
    PACKAGES = "switch-meta openbox switch-xorg-touchscreen-rule nvidia-container-toolkit onboard chromium-nvdec nvidia-l4t-bsp-7.4"
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^initial-setup|^sddm-wayland-plasma' | xargs rpm -e --noscripts --nodeps",
        "usermod -aG video,audio,input sddm"
    ]

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.update()
            self.remove("qemu-user-static uboot-images-armv8 kernel-devel httpd imsettings akregator kamoso mediawriter elisa-player kmag kgpg qt5-qdbusviewer kcharselect kcolorchooser dragon kmines kmahjongg kpat kruler kmousetool kmouth kolourpaint konversation krdc kfind kaddressbook kmail kontact korganizer ktnef krfb kf5-akonadi-*")
            for url in self.REPOS:
                self.addRepo(url)

            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")

            self.swap("kernel*", "switch-kernel*")
            self.installPackages()
            self.update()
            self.run("systemctl enable initial-setup l4t-firstboot sddm-postlogin")
            self.cleanup()

            if os.environ.get("JETFACTORY_DEBUG") is not None:
                self.run("systemctl disable initial-setup l4t-firstboot")
                self.run("adduser -s /bin/bash -m -G wheel,video,audio,input,kvm fedora")
                self.run("echo 'fedora:fedora' | chpasswd")
                self.run("echo -e '[Autologin]\nUser=fedora\nSession=plasmax11' > /etc/sddm.conf.d/autologin.conf")
                self.run("echo 'uart_port=2' >> /.boot/bootloader/ini/L4T-fedora.ini")
                self.install("vim memtester")

class FedorakdenouveauSwitch(FedoraSwitch):
    ''' Fedora KDE Switch Nouveau kernel '''
    name = "fedoranouveau"
    rootfs = "kde"
    url = "https://download.fedoraproject.org/pub/fedora-secondary/releases/39/Spins/aarch64/images/Fedora-KDE-39-1.5.aarch64.raw.xz"
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_39/home:Azkali:Tegra:Icosa.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa_testing/Fedora_39/home:Azkali:Tegra:Icosa_testing.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra/Fedora_39/home:Azkali:Tegra.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Apps/Fedora_39/home:Azkali:Apps.repo"
    ]
    PRE = [
        "echo 'exclude=kernel-core*,kernel-modules*,mesa*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^mesa' | xargs rpm -e --noscripts --nodeps"
    ]
    PACKAGES = "switch-mainline-meta qt5-qtvirtualkeyboard"

    def build(self):
        LinuxFactory.build(self)
        self.splitImage()
        create_archive(self.zip_name, [self.distro_dir + "/switchroot/", self.distro_dir + "/bootloader/"])

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.remove("uboot-images-armv8 kernel-devel httpd imsettings akregator kamoso mediawriter elisa-player kmag kgpg qt5-qdbusviewer kcharselect kcolorchooser dragon kmines kmahjongg kpat kruler kmousetool kmouth kolourpaint konversation krdc kfind kaddressbook kmail kontact korganizer ktnef krfb kf5-akonadi-*")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.swap("kernel*", "switch-kernel*")
            self.swap("sddm-x11", "sddm-wayland-plasma")
            self.swap("initial-setup*", "l4t-initial-setup*")
            self.installPackages()
            self.cleanup()
            if os.environ.get("JETFACTORY_DEBUG") is not None:
                self.run("systemctl disable initial-setup")
                self.run("adduser -s /bin/bash -m -G wheel,video,audio,input,kvm fedora")
                self.run("echo 'fedora:fedora' | chpasswd")
                self.run("echo -e '[Autologin]\nUser=fedora\nSession=plasma' > /etc/sddm.conf.d/autologin.conf")
                self.run("echo -e '[General]\nDisplayServer=wayland\nGreeterEnvironment=QT_WAYLAND_SHELL_INTEGRATION=layer-shell\n\n[Wayland]\nCompositorCommand=kwin_wayland --drm --no-lockscreen --no-global-shortcuts --locale1 --inputmethod maliit-keyboard' > /etc/sddm.conf.d/wayland.conf")

class MainsailSwitch(FedorakdeSwitch):
    ''' Fedora Switch KDE Mainsail '''
    POST = [
        "adduser -s /bin/bash -m -G wheel,video,audio,input,kvm,docker,dialout,tty fedora",
        "echo 'fedora:fedora' | chpasswd",
        "echo -e '[Autologin]\nUser=fedora\nSession=plasmax11' > /etc/sddm.conf.d/autologin.conf",
        "cd /opt && git clone https://github.com/mkuf/prind && git clone https://github.com/bassamanator/Sovol-SV06-firmware",
        "cp -r Sovol-SV06-firmware/{cfgs,osskc.cfg,printer.cfg} prind/configs/",
        "sed -i 's/\/home\/pi/\/opt/g' prind/configs/cfgs/misc-macros.cfg",
        "sed -i 's/serial: .*/serial: \/dev\/serial\/by-id\/usb-1a86_USB_Serial-if00-port0/g' prind/configs/printer.cfg",
        "rm -rf Sovol-SV06-firmware",
        "wget https://github.com/docker/compose/releases/download/v2.23.3/docker-compose-linux-aarch64 -O /usr/local/lib/docker/cli-plugins/docker-compose",
        "chmod +x /usr/local/lib/docker/cli-plugins/docker-compose",
        '''echo "ACTION=="add",SUBSYSTEM=="tty",ATTRS{idVendor}=="1a86",ATTRS{idProduct}=="7523",GROUP="20" > /etc/udev/rules.d/klipper_sv06.rules''',
        '''echo "ACTION=="add", KERNEL=="video[0-9]*", ATTR{idVendor}=="046d", ATTR{idProduct}=="0825", MODE="0666"" > /etc/udev/rules.d/c270.rules''',
        "wget https://raw.githubusercontent.com/mainsail-crew/mainsail/develop/public/img/logo.svg -O /usr/share/icons/mainsail.svg",
        "echo -e '[Desktop Entry]\nType=Application\nName=MainSail\nIcon=mainsail.svg\nExec=chromium-browser-stable --app=http://localhost:80 --start-fullscreen\nCategories=Science' > /etc/xdg/autostart/mainsail.desktop",
        "echo -e '[Desktop Entry]\nType=Application\nName=MainSail\nIcon=mainsail.svg\nExec=chromium-browser-stable --app=http://localhost:80 --start-fullscreen\nCategories=Science' > /usr/share/applications/mainsail.desktop",
        "echo -e '[Unit]\nDescription=Mainsail docker compose\nRequires=docker.service\nAfter=docker.service\n[Service]\nType=oneshot\nRemainAfterExit=yes\nWorkingDirectory=/opt/prind\nExecStart=docker-compose --profile mainsail up -d\nExecStop=docker-compose down\nTimeoutStartSec=0\n[Install]\nWantedBy=multi-user.target' > /etc/systemd/system/mainsail.service",
        "firewall-offline-cmd --permanent --zone=FedoraWorkstation --add-service=http",
        "systemctl enable docker docker.socket mainsail"
    ]

    def createRootfs(self):
        with self.chroot():
            self.installRpmFusion()
            self.preConfigs()
            self.update()
            self.remove("firefox uboot-images-armv8 kernel-devel httpd imsettings akregator kamoso mediawriter elisa-player kmag kgpg qt5-qdbusviewer kcharselect kcolorchooser dragon kmines kmahjongg kpat kruler kmousetool kmouth kolourpaint konversation krdc kfind kaddressbook kmail kontact korganizer ktnef krfb kf5-akonadi-* brltty")
            for url in self.REPOS:
                self.addRepo(url)
            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            self.swap("kernel*", "switch-kernel*")
            self.installPackages()
            self.swap("nvidia-l4t-bsp", "nvidia-l4t-bsp-7.4")
            self.postConfigs()
            self.cleanup()

class ProxmoxSwitch(Switch, Debian):
    PRE = [
            "ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime",
            "TZ=America/New_York ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone",
            'mkdir -p /etc/apt/keyrings/',
            'wget https://raw.githubusercontent.com/theofficialgman/l4t-debs/main/pgp-key.public -O /etc/apt/keyrings/theofficialgman-L4T.asc',
            'echo "deb [signed-by=/etc/apt/keyrings/theofficialgman-L4T.asc] https://theofficialgman.github.io/l4t-debs/ l4t jammy" > /etc/apt/sources.list.d/theofficialgman-L4T.list',
            'echo "deb https://global.mirrors.apqa.cn/proxmox/debian/pve bullseye port" > /etc/apt/sources.list.d/pveport.list',
            'curl https://global.mirrors.apqa.cn/proxmox/debian/pveport.gpg -o /etc/apt/trusted.gpg.d/pveport.gpg',
            'mkdir -p /opt/nvidia/l4t-packages/',
            'touch /opt/nvidia/l4t-packages/.nv-l4t-disable-boot-fw-update-in-preinstall'
    ]
    pkg2 = "switch-dock-handler switch-alsa-ucm2 switch-touch-rules switch-l4t-configs switch-dconf-customizations switch-joystick-mouse switch-bsp switch-l4t-repo ifupdown2"
    OPTIONS = "-o Dpkg::Options::=--force-confnew"

    def prepare(self):
        LinuxFactory.prepare(self)
        Switch.prepare(self)
        Debian.prepare(self)

    def build(self):
        ''' Create a 7z archive containing a splitted image file to fit on switch's fat32 PARTITION '''
        LinuxFactory.build(self)
        self.splitImage()
        create_archive(self.zip_name, [self.distro_dir + "/switchroot/", self.distro_dir + "/bootloader/"])

    def createRootfs(self):
        with self.chroot():
            self.preConfigs()
            self.update()
            self.install(self.pkg2)
            self.install("proxmox-ve postfix open-iscsi")

            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
