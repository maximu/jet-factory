import os
from jetfactory.jetfactory import LinuxFactory
from jetfactory.rootfs import Fedora
from sh import (
    zerofree,
    virt_make_fs,
    guestfish,
    dd
)

class Rock5b(LinuxFactory):
    device = "rock5b"

    def prepare(self):
        LinuxFactory.prepare(self)

class FedoraRock5b(Rock5b, Fedora):
    name = "fedora"
    PACKAGES = "rk-meta"
    REPOS = [
        "https://download.opensuse.org/repositories/home:/Azkali:/RockPi5b/Fedora_37/home:Azkali:RockPi5b.repo",
        "https://download.opensuse.org/repositories/home:/Azkali:/Tegra:/Icosa/Fedora_37/home:Azkali:Tegra:Icosa.repo"
    ]
    PRE = [
        "touch /.initial-setup",
        "echo 'exclude=kernel-core*,kernel-modules*,mesa*,libglvnd*' >> /etc/dnf/dnf.conf",
        "rpm -qa | grep -E '^kernel|^mesa|^libglvnd' | xargs rpm -e --noscripts --nodeps"
    ]
    ROOT_UUID = "614e0000-0000-4b53-8000-1d28000054a9"
    LOADER1_START = "64"
    LOADER2_START = "16384"
    BOOT_START = "32768"
    ROOT_START = 1081344

    def prepare(self):
        Rock5b.prepare(self)
        Fedora.prepare(self)

    def createSystemImage(self):
        self.logger.info("Creating boot.img")
        virt_make_fs(
            self.distro_dir + "/boot/",
            self.build_dir + "/boot.img",
            "--blocksize=512",
            "-t", "vfat",
            _log_msg=self.sh_log
        )

        boot_size = os.path.getsize(self.build_dir + "/boot.img")
        root_size = os.path.getsize(self.disk_name)
        idb_size = os.path.getsize(self.distro_dir + "/boot/idbloader.img")
        uboot_size = os.path.getsize(self.distro_dir + "/boot/u-boot.itb")
        total_size = ((boot_size + root_size + idb_size + uboot_size + 805306368) >> 20)
        print(total_size)

        self.logger.info("Creating system.img")
        guestfish(
            "-N",
            self.build_dir + "system.img=disk:" + str(total_size) + "M",
            _log_msg=self.sh_log
        )

        self.logger.info("Creating guestfish script")
        with open(self.build_dir + "/guestfish.sh", "w") as gfish:
            gfish.write("#!/usr/bin/guestfish -f\n")
            gfish.write("add " + self.build_dir + "system.img\n")
            gfish.write("add " + self.build_dir + "boot.img\n")
            gfish.write("add " + self.disk_name + "\n")
            gfish.write("run\n")
            gfish.write("part-init /dev/sda gpt\n")
            gfish.write("part-add /dev/sda p " + self.BOOT_START +  " " + str(self.ROOT_START - 1) + "\n")
            gfish.write("part-add /dev/sda p " + str(self.ROOT_START) + " -17408\n") # 17048 is 34s * 512 (sector size)
            gfish.write("copy-device-to-device /dev/sdb /dev/sda1\n")
            gfish.write("copy-device-to-device /dev/sdc /dev/sda2\n")
            gfish.write("part-set-name /dev/sda 1 boot\n")
            gfish.write("part-set-bootable /dev/sda 1 true\n")
            gfish.write("part-set-name /dev/sda 2 rootfs\n")
            gfish.write("part-set-gpt-guid /dev/sda 2 " + self.ROOT_UUID + "\n")
        os.chmod(self.build_dir + "/guestfish.sh", 0o775)

        self.logger.info("Flashing boot.img and rootfs.img to system.img")
        self.run(self.build_dir + "/guestfish.sh")
        os.remove(self.build_dir + "/guestfish.sh")

        self.logger.info("Flashing idbloader.img to system.img")
        dd(
            "if=" + self.distro_dir + "/boot/idbloader.img",
            "of=" + self.build_dir + "system.img",
            "seek=" + self.LOADER1_START,
            "conv=notrunc"
        )

        self.logger.info("Flashing u-boot.itb to system.img")
        dd(
            "if=" + self.distro_dir + "/boot/u-boot.itb",
            "of=" + self.build_dir + "system.img",
            "seek=" + self.LOADER2_START,
            "conv=notrunc"
        )
        os.remove(self.build_dir + "boot.img")
        os.remove(self.disk_name)

    def build(self):
        LinuxFactory.build(self)
        self.createSystemImage()

    def createRootfs(self):
        with self.chroot():
            self.run(self.MANAGER + " " + self.INSTALL + " https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm")
            self.run(self.MANAGER + " " + self.INSTALL + " https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm")
            self.preConfigs()
            for url in self.REPOS:
                self.addRepo(url)

            if os.environ.get("JETFACTORY_INTERACTIVE") is not None:
                self.run("/bin/bash")
            else:
                self.installPackages()
