from argparse import ArgumentParser
import importlib
import time
import os
import glob
import inspect

# Argument parser
parser = ArgumentParser()
parser.add_argument(
    "-b", "--build", dest="build", nargs='+',
    help="-b, --build [DEVICE] [ROOTFS]")
parser.add_argument(
    "-d", "--debug", dest="debug",
    default=False, action="store_true",
    help="-d, --debug Enable debug logging")
parser.add_argument(
    "-i", "--interactive", dest="interactive",
    default=False, action="store_true",
    help="-i, --interactive Perform a simple interactive chroot")
args = parser.parse_args()

# Set debug variables
if args.debug:
    os.environ['LIBGUESTFS_DEBUG'] = "1"
    os.environ['LIBGUESTFS_TRACE'] = "1"
    os.environ['JETFACTORY_DEBUG'] = "True"

if args.interactive:
    os.environ['JETFACTORY_INTERACTIVE'] = "True"

# Find all devices
modules = glob.glob(
    os.path.join(
        os.path.dirname(
            os.path.realpath(__file__)) + "/devices/**/", "*.py"))

# Match devices found with device to use
query = next(
    filter(
        lambda x: args.build[0] in x, modules)).split("jetfactory/")
mod_path = os.path.dirname(query[1]).replace("/", ".")
device = "jetfactory." + mod_path + "." + args.build[0]

# Import our module
try:
    module = importlib.import_module(device)

except ImportError as err:
    print(err)
    print("Unknown device named: {}\n".format(args.build[0]))
    parser.print_help()
    parser.exit()

# Compose rootfs name
rootfs = args.build[1].capitalize() + args.build[0].capitalize()

# Search for a rootfs implementation
for name, cls in inspect.getmembers(module, inspect.isclass):
    if name == rootfs:
        rootClass = getattr(module, rootfs)

# Finally run the class
start = time.time()
try:
    rootClass()
except Exception as err:
    print(err)
    parser.exit()

print("Build finished in {}"
    .format(
        time.strftime(
            "%Hh%Mm%Ss",
            time.gmtime(
                time.time() - start))))
