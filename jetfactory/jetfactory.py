#!/usr/bin/env python
from datetime import datetime
from shutil import rmtree, copy
import os
from concurrent.futures import ThreadPoolExecutor
import signal
from functools import partial
from threading import Event
from typing import Iterable
from urllib.request import urlopen, Request
from patoolib import extract_archive
from subprocess import Popen
from sys import stdout, stderr
from contextlib import contextmanager
import logging
from rich.logging import RichHandler
import abc
from platform import processor
from tempfile import mkdtemp
from glob import glob
from rich.progress import (
    BarColumn,
    DownloadColumn,
    Progress,
    TaskID,
    TextColumn,
    TimeRemainingColumn,
    TransferSpeedColumn,
)
from sh import (
        debootstrap,
        mount,
        umount,
        zerofree,
        mkfs,
        rsync,
        losetup,
        kpartx,
        blkid,
        parted,
        dd
)

IMAGE_EXTENSIONS = ['.img', '.qcow2', '.raw']
ARCHIVE_EXTENSIONS = ['.tar.gz', '.bz2', '.tbz2', '.xz', '.tar', '.tar.xz', '.7z']

class LinuxFactory:
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        # Initialize the logger
        FORMAT = "%(message)s"
        logging.basicConfig(
            level="INFO",
            format=FORMAT,
            datefmt="[%X]",
            handlers=[RichHandler(rich_tracebacks=True)]
        )
        self.logger = logging.getLogger("rich")
        self.logger.info("Building {} for {}".format(self.name, self.device))
        self.prepare()

    def __del__(self):
        rmtree(self.distro_dir, ignore_errors=True)
        rmtree(self.chroot_dir, ignore_errors=True)

    @abc.abstractmethod
    def prepare(self):
        self.logger.info("Preparing build environment")
        date = datetime.today().strftime('%Y-%m-%d')

        # Persistent dirs
        self.build_dir = os.getcwd() + "/linux/"
        self.dl_dir = self.build_dir + "downloadedFiles/"
        os.makedirs(self.dl_dir, exist_ok=True)

        # Temporary dirs
        self.distro_dir = mkdtemp(suffix="-distro")
        self.chroot_dir = mkdtemp(suffix="-chroot")
        self.disk_dir = mkdtemp(suffix="-mnt")

        # Disk image and zip
        filename = self.device + "-" + self.rootfs + "-" + date
        self.disk_name = self.build_dir + filename + ".img"
        self.zip_name = self.build_dir + filename + ".7z"

        if os.path.exists(self.zip_name):
            self.logger.info("Cleaning up old zip")
            os.remove(self.zip_name)

    def build(self):
        self.makeDiskImage()

    def sh_log(self, ran, call_args, pid=None):
        return self.logger.info("{}".format(ran))

    def extract(self, fd, out):
        ''' Rootfs extract helper '''
        self.logger.info("Extracting rootfs from {}.".format(fd))

        if fd.endswith(tuple(ARCHIVE_EXTENSIONS)):
            extract_archive(fd, outdir=out)

        elif fd.endswith(tuple(IMAGE_EXTENSIONS)):
            # Remove all previous loop devices
            [kpartx("-dv", loop) for loop in glob("/dev/loop[0-9]*") if loop]
            losetup("-D")

            # Setup new loop device
            loop = losetup("--show", "-P", "-f", fd).strip()
            kpartx("-av", loop)

            # Get disk number, assuming rootfs is last
            disk = max(glob(loop[:5] + 'mapper/' + loop[5:] + "p[0-9]*"))

            # Mount root
            # TODO: Don't assume btrfs
            # Get disk type specifically for btrfs volume
            #disktype = dict(item.split("=") for item in blkid("-p", disk).split(": ")[1].split(" "))
            mount(disk, "-o", "subvol=root", self.disk_dir)

            # Copy
            rsync("-a", self.disk_dir + "/", out)

            # Unmount
            umount(self.disk_dir)
            [kpartx("-dv", loop) for loop in glob("/dev/loop[0-9]*") if loop]
            losetup("-D")

        else:
            self.logger.exception("Unsupported file format for {}".format(fd))
        self.logger.info("Extracted {} successfully".format(fd))

    def extract_rootfs(self, rootfs, out = ""):
        ''' Image or archive extraction helper '''
        out = out if out else self.chroot_dir
        extracted_image = self.dl_dir + os.path.basename(os.path.splitext(rootfs)[0])

        for arch in ARCHIVE_EXTENSIONS:
            for img in IMAGE_EXTENSIONS:
                if rootfs.endswith(img + arch):
                    self.logger.info("Found compressed disk image {}".format(rootfs))
                    if not os.path.exists(extracted_image):
                        self.extract(rootfs, self.dl_dir)
                    self.extract(extracted_image, out)
                    return

        if os.path.exists(extracted_image):
            self.extract(extracted_image, out)
        else:
            self.extract(rootfs, out)

    def makeDiskImage(self):
        ''' EXT4 disk image creation '''
        self.logger.info("Making rootfs add an extra 256MB")

        # Calculate the size of the directory
        total_size = sum(os.path.getsize(os.path.join(dirpath, f))
                       for dirpath, dirnames, filenames in os.walk(self.chroot_dir)
                       for f in filenames if not os.path.islink(os.path.join(dirpath, f)))

        # Add an extra 1GB to the size
        disk_size = str(int((total_size)/1024/1024) + 1024)
        label = 'SWR-' + self.name[:3].upper()

        # Create disk image
        dd("if=/dev/zero", "of=" + self.disk_name, "bs=1M", "count=" + disk_size, "conv=fsync", "status=progress")
        parted("-s", self.disk_name, "mklabel", "msdos")
        parted("-s", self.disk_name, "mkpart", "primary", "ext4", "0%", "100%")
        mkfs("-t", "ext4", "-L", label, self.disk_name)

        # Copy files
        mount(self.disk_name, self.disk_dir)
        rsync("-a", self.chroot_dir + "/", self.disk_dir)

        umount(self.disk_dir)
        zerofree(self.disk_name)

    def pymount(self, *args):
        mount(args, _log_msg=self.sh_log)

    def pyumount(self, *args):
        umount(args, _log_msg=self.sh_log)

    @contextmanager
    def chroot(self):
        ''' Chroot '''
        self.root = self.chroot_dir

        def mountDir(path):
            mountedDir = self.root + path
            if not os.path.ismount(mountedDir):
                os.makedirs(mountedDir, exist_ok=True)
                if path == "/dev":
                    self.pymount(mnt, mountedDir, "--bind")
                elif path == "/proc":
                    self.pymount(mnt, mountedDir, "-t", "proc")
                elif path == "/sys":
                    self.pymount(mnt, mountedDir, "-t", "sysfs")

        def umountDir(path):
            mountedDir = self.root + path
            if os.path.ismount(mountedDir):
                if path == "/dev":
                    self.pyumount("-lf", mountedDir)
                else:
                    self.pyumount(mountedDir)

        def cleanupChroot(real_root):
            os.fchdir(real_root)
            os.chroot(".")
            os.close(real_root)

            self.logger.info("Syncing disks before unmount")
            os.sync()

            # Device hooks
            if self.device == "switch":
                self.pyumount(self.root + "/boot/switchroot")
                self.pyumount(self.root + "/.boot/bootloader")
            elif self.device == "rock5b":
                self.pyumount(self.root + "/boot/")

            for mnt in ('/proc', '/dev', '/sys'):
                umountDir(mnt)
            if hasattr(self, 'cache'):
                self.pyumount(self.root + self.cache)

        self.logger.info("Chrooting to {}".format(self.root))
        self.logger.info("Keeping a reference to host root")
        real_root = os.open("/", os.O_RDONLY)

        # Resolv.conf
        self.logger.info("Copying resolv.conf from host to guest for dns resolution")
        if os.path.islink(self.root + "/etc/resolv.conf"):
            os.unlink(self.root + "/etc/resolv.conf")
        copy("/etc/resolv.conf", self.root + "/etc/resolv.conf")

        # Special FS
        try:
            for mnt in ('/proc', '/dev', '/sys'):
                mountDir(mnt)
        except:
            self.logger.exception("Special device mounting failed")
            exit()

        # Device Hooks
        try:
            if self.device == "switch":
                os.makedirs(self.root + "/boot/switchroot", exist_ok=True)
                mount(self.distro_dir + "/switchroot/" + self.name, self.root + "/boot/switchroot", "--bind")
                os.makedirs(self.root + "/.boot/bootloader", exist_ok=True)
                os.makedirs(self.distro_dir + "/bootloader", exist_ok=True)
                mount(self.distro_dir + "/bootloader/", self.root + "/.boot/bootloader", "--bind")
            elif self.device == "rock5b":
                os.makedirs(self.root + "/boot/", exist_ok=True)
                os.makedirs(self.distro_dir + "/boot/", exist_ok=True)
                mount(self.distro_dir + "/boot/", self.root + "/boot/", "--bind")
        except:
            cleanupChroot(real_root)
            self.logger.exception("Mounting boot dir for {} failed".format(self.device))
            exit()

        if hasattr(self, 'cache'):
            os.makedirs(self.build_dir + ".cache/", exist_ok=True)
            os.makedirs(self.root + self.cache, exist_ok=True)
            mount(self.build_dir + ".cache/", self.root + self.cache, "--bind")

        try:
            os.setgid(0)
            os.setuid(0)
            os.chroot(self.root)
            os.chdir('/')
            yield
        except:
            cleanupChroot(real_root)
            self.logger.exception("Chroot failed, cleaning up")
            exit()

        cleanupChroot(real_root)

    def download(self, urls: Iterable[str], dest_dir: str):
        """
            A rudimentary URL downloader (like wget or curl) to demonstrate Rich progress bars.
            Download multiple files to the given directory.
        """
        progress = Progress(
            TextColumn("[bold blue]{task.fields[filename]}", justify="right"),
            BarColumn(bar_width=None),
            "[progress.percentage]{task.percentage:>3.1f}%",
            "•",
            DownloadColumn(),
            "•",
            TransferSpeedColumn(),
            "•",
            TimeRemainingColumn(),
        )

        done_event = Event()

        def handle_sigint(signum, frame):
            done_event.set()

        signal.signal(signal.SIGINT, handle_sigint)

        def copy_url(task_id: TaskID, url: str, path: str) -> None:
            """Copy data from a url to a local file."""
            progress.console.log(f"Requesting {url}")
            response = urlopen(url)
            # This will break if the response doesn't contain content length
            progress.update(task_id, total=int(response.info()["Content-length"]))
            with open(path, "wb") as dest_file:
                progress.start_task(task_id)
                for data in iter(partial(response.read, 32768), b""):
                    dest_file.write(data)
                    progress.update(task_id, advance=len(data))
                    if done_event.is_set():
                        return
            progress.console.log(f"Downloaded {path}")

        with progress:
            with ThreadPoolExecutor(max_workers=4) as pool:
                for url in urls:
                    headers = {
                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
                        "Accept-Encoding": "gzip, deflate, br",
                        "Accept-Language": "en-US,en;q=0.9,fr;q=0.8",
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36",
                    }

                    header = urlopen(Request(url, method="HEAD", headers=headers))
                    filename = header.info().get_filename() if header.info().get_filename() else url.split("/")[-1]

                    self.logger.info("Downloading {}".format(filename))
                    req = Request(url, headers=headers)
                    dest_path = os.path.join(dest_dir, filename)
                    task_id = progress.add_task("download", filename=filename, start=False)
                    pool.submit(copy_url, task_id, req, dest_path)

        return dest_path

    def run(self, cmd):
        ''' subprocess.Popen wrapper '''
        out, err = Popen(
            cmd,
            universal_newlines=True,
            shell=True,
            stdout=stdout,
            stderr=stderr
        ).communicate()

        if err is not None:
            self.logger.exception(err)
        return out
