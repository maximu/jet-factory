# Jet Factory

Create live and flashable linux distribution root filesystem images for L4T.

## Usage

```txt
usage:
  -b, --build   <device> <rootfs>	
  -h, --help    show this help message and exit
```

## Dependencies

Required :
```
sudo apt install -y libusb-1.0-0 binfmt-support qemu-user-static p7zip-full util-linux zerofree git unrar unzip python3 python3-pip libguestfs-tools libguestfs-dev libguestfs-gobject-dev libguestfs-gobject-1.0-0 xz-utils build-essential
```

Optional (required for Switch builds only):
```
sudo apt install -y u-boot-tools cpio gzip device-tree-compiler make git build-essential gcc bison flex python3 python3-dev swig python python-dev bc curl libncurses5-dev m4 zlib1g-dev p7zip-full wget acpica-tools
```

## Build example

```sh
sudo docker run --pull always --privileged --rm -it -v "$PWD"/linux:/build/linux registry.gitlab.com/l4t-community/gnu-linux/jet-factory:master -b <device> <rootfs>
```

## Available devices|rootfs:

|               | switch| t194 | t234 | t210 |
|---------------|-------|------|------|------|
| fedora        |  Yes  |  Yes |  Yes |  Yes |
| fedoranouveau |  Yes  |  No  |  No  |  No  |

## Credits

### Special mentions

@gavin_darkglider, @CTCaer, @ByLaws, @ave and all the L4S & [switchroot](https://switchroot.org) members \
For their various work and contributions to switchroot.

### Contributors

@Stary2001, @Kitsumi, @parkerlreed, @AD2076, @PabloZaiden, @andrebraga1 \
@nicman23 @Matthew-Beckett @Nautilus @3l_H4ck3r_C0mf0r7
For their work, support and direct contribution to this project.
