FROM ubuntu:noble
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt-get install -y gnupg2 && \
	    apt install -y \
	    p7zip-full \
	    util-linux \
	    zerofree \
	    unrar \
	    unzip \
	    python3 \
	    python3-pip \
	    xz-utils \
	    build-essential \
	    linux-image-generic \
	    cpio \
	    gzip \
	    git \
	    python3-dev \
	    python-is-python3 \
	    wget \
	    lz4 \
	    coreutils \
	    rsync \
	    debian-archive-keyring \
	    debootstrap \
	    kpartx \
	    parted \
	    sudo

RUN rm -rf /var/lib/apt/lists/*

WORKDIR /build
VOLUME /build

COPY . /jet
ENV PATH=/root/.local/bin:$PATH
RUN pip3 install --user -e /jet/ --break-system-packages

ENTRYPOINT ["python3", "-m", "jetfactory"]
CMD [""]
